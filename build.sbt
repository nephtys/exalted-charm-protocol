import sbt.Keys._

name := "exalted-charm-protocol Root"

scalaVersion in ThisBuild := "2.11.8"



lazy val root = project.in(file(".")).
  aggregate(charmprotocolJS, charmprotocolJVM).
  settings(
    publish := {},
    publishLocal := {}
  )

lazy val charmprotocol = crossProject.in(file(".")).
  settings(
    name := "exalted-charm-protocol",
    organization := "org.bitbucket.nephtys",
    version := "0.1-SNAPSHOT",

      libraryDependencies += "org.scalactic" %% "scalactic" % "3.0.0",
libraryDependencies += "org.scalatest" %% "scalatest" % "3.0.0" % "test",
libraryDependencies += "com.lihaoyi" %% "upickle" % "0.4.3"

  ).
  jvmSettings(
    // Add JVM-specific settings here
libraryDependencies += "org.scala-js" %% "scalajs-stubs" % scalaJSVersion % "provided"
  ).
  jsSettings(
    // Add JS-specific settings here
  )

lazy val charmprotocolJVM = charmprotocol.jvm
lazy val charmprotocolJS = charmprotocol.js
