package nephtys.loom.charm.protocol

import scala.util.{Failure, Success}

/**
  * Created by nephtys on 12/2/16.
  */
object State {

  def Nil = Map.empty[UniquePowerIdentifier, Power]

  def commit(events : CharmProtocol.Event*) : Map[UniquePowerIdentifier, Power] = {
    var state = Nil
    events.foreach(e => state = e.commit(state))
    state
  }

  def verifyAndCommit(sender : Email, commands : CharmProtocol.Command*) : Map[UniquePowerIdentifier, Power] = {
    verifyAndCommitSeq(sender, commands)
  }

  def verifyAndCommitSeq(sender : Email, commands : Seq[CharmProtocol.Command]) = {
    var state = Nil
    commands.foreach(command => {
      command.validate(sender, state) match {
        case Success(event) => state = event.commit(state)
        case Failure(e) => println(s"Command $command failed to verify: ${e.getMessage}")
      }
    })
    state
  }

}
