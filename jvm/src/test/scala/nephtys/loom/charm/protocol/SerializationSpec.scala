package nephtys.loom.charm.protocol

import org.scalatest.{FlatSpec, Matchers}
import java.util.UUID

import org.scalatest._

/**
  * Created by nephtys on 11/15/16.
  */
class SerializationSpec extends FlatSpec with Matchers{
  import CharmProtocol._

  import upickle.default._


  val sender = Email("owner@email.address")

  "A rename and description and keyword change" should "be serialized successfully" in {

    val id = UniquePowerIdentifier(UUID.randomUUID().toString)
    val create = Create(sender, "Charm 1", PowerType.Charm, id, None)
    val rename = SetName(id,"ABC")
    val descrpt = SetDescription(id, "This is a description")
    val keywordAdd1 = AddKeyword(id, "keyword-A")
    val keywordAdd2 = AddKeyword(id, "keyword-B")
    val keywordRemove = RemoveKeyword(id, keywordAdd1.keyword)

    val p = State.verifyAndCommit(sender, create, rename, descrpt,keywordAdd1, keywordAdd2, keywordRemove)

    val seq : Seq[Command] = Seq(create, rename, descrpt,keywordAdd1, keywordAdd2, keywordRemove)
    val serial : String = write(seq)
    println(serial)
    val red : Seq[Command] = read[Seq[Command]](serial)
    red should be (seq)
    val reread = State.verifyAndCommitSeq(sender, red)

    reread should be (p)
  }

  "A description change should be serialized and deseralized correctly" should "be serialized succesfully" in {
    val uuid = UniquePowerIdentifier("3dec2e7a-4077-4ff3-8495-3ae6cf8e0e47")
    println(uuid)
    val shouldJson : String = """{"$type":"nephtys.loom.charm.protocol.CharmProtocol.DescriptionChanged","id":{"id":"3dec2e7a-4077-4ff3-8495-3ae6cf8e0e47"},"description":"new description"}"""
    println(shouldJson)
    val event = DescriptionChanged(uuid, "new description")
    val serial = write(event)
    println(serial)
    val deserial = read[Event](shouldJson)
    serial should be (shouldJson)
    deserial should be (event)
    deserial.isInstanceOf[DescriptionChanged] should be (true)
    deserial.isInstanceOf[NameChanged] should be (false)

  }

  "A list of changes" should "be serialized succesffully" in {
    val uuid = UniquePowerIdentifier("3dec2e7a-4077-4ff3-8495-3ae6cf8e0e47")
    println("list of events:")
    val seq : Seq[Event] = Seq(DescriptionChanged(uuid, "new description"), NameChanged(uuid, "new name"))
    val json : String  = ""
    val serial : String  = write(seq)
    println(serial)
    serial should be (json)
    val deserial : Seq[Event] = read[Seq[Event]](json)
    deserial should be (seq)

  }

  "A Charm" should "be serialized successfully" in {
    val id = UniquePowerIdentifier(UUID.randomUUID().toString)
    val create = Create(sender, "Charm 1", PowerType.Charm, id, None)
    val rename = SetName(id,"ABC")
    val descrpt = SetDescription(id, "This is a description")
    val keywordAdd1 = AddKeyword(id, "keyword-A")
    val keywordAdd2 = AddKeyword(id, "keyword-B")
    val keywordRemove = RemoveKeyword(id, keywordAdd1.keyword)

    val p = State.verifyAndCommit(sender, create, rename, descrpt,keywordAdd1, keywordAdd2, keywordRemove)

    val serial = write(p)
    println(serial)
    val reread = read[Map[UniquePowerIdentifier, Power]](serial)
    reread should be (p)
  }

}
