package nephtys.loom.charm.protocol

import java.util.UUID

import org.scalatest._


/**
  * Created by nephtys on 11/14/16.
  */
class ProtocolSpec  extends FlatSpec with Matchers {
  import CharmProtocol._
  val sender = Email("owner@email.address")

  "A creation" should "create a new instance" in {
    val uuid = UniquePowerIdentifier(UUID.randomUUID().toString)
    val command = Create(sender, "Charm 1", PowerType.Charm, uuid, None)
    val event = Creation(sender, "Charm 1", PowerType.Charm, uuid, None)

    State.verifyAndCommit(sender, command) should be (State.commit(event))
    State.verifyAndCommit(sender, command).size == 1
    State.verifyAndCommit(sender, command)(uuid).name == event.name
  }

  "A doubled creation" should "not be allowed" in {
    val uuid = UniquePowerIdentifier(UUID.randomUUID().toString)
    val command1 = Create(sender, "Charm 1", PowerType.Charm, uuid, None)
    val command2 = Create(sender, "Charm 2", PowerType.Charm, uuid, None)

    State.verifyAndCommit(sender, command1) should be (State.verifyAndCommit(sender, command1, command2))
  }

  "A creation and deletion" should "cancel each other out" in {
    val uuid = UniquePowerIdentifier(UUID.randomUUID().toString)
    val command1 = Create(sender, "Charm 1", PowerType.Charm, uuid, None)
    val command2 = Delete(uuid)

    State.verifyAndCommit(sender, command1, command2) should be (State.Nil)
  }

  "A rename and description and keyword change" should "be possible" in {
    val id = UniquePowerIdentifier(UUID.randomUUID().toString)
    val create = Create(sender, "Charm 1", PowerType.Charm, id, None)
    val rename = SetName(id,"ABC")
    val descrpt = SetDescription(id, "This is a description")
    val keywordAdd1 = AddKeyword(id, "keyword-A")
    val keywordAdd2 = AddKeyword(id, "keyword-B")
    val keywordRemove = RemoveKeyword(id, keywordAdd1.keyword)

    val p = State.verifyAndCommit(sender, create, rename, descrpt,keywordAdd1, keywordAdd2, keywordRemove)
    p.size should be (1)
    p(id).keywords should be (Set(Keyword(keywordAdd2.keyword)))
    p(id).name should be (rename.name)
    p(id).description should be (Description(descrpt.description))
  }
}