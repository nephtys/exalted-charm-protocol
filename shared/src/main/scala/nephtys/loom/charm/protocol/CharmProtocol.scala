package nephtys.loom.charm.protocol

import scala.scalajs.js.annotation.JSExportAll
import scala.util.{Failure, Success, Try}
/**
  * Created by nephtys on 11/3/16.
  */
@JSExportAll
object CharmProtocol extends Protocol[Map[UniquePowerIdentifier, Power]] {



  //this is a hack to allow sealed traits


  sealed trait Command {
    def validate(user : Email, aggregate : Map[UniquePowerIdentifier, Power]) : Try[Event] = {
      if (verifyAccess(user, aggregate)) {
        validateInternal(aggregate)
      } else {
        Failure(new IllegalAccessException)
      }
    }
    protected def validateInternal(aggregate : Map[UniquePowerIdentifier, Power]) : Try[Event]
    def id : UniquePowerIdentifier

    //the following method is used for authorization on commands
    def verifyAccess(user : Email, aggregate : Map[UniquePowerIdentifier, Power]) : Boolean = {
      aggregate.get(id).exists(_.owner.email == user.email)
    }
  }

  sealed trait Event {
    def commit(aggregate : Map[UniquePowerIdentifier, Power]) : Map[UniquePowerIdentifier, Power]
    def id : UniquePowerIdentifier
  }


  case class Create(owner : Email, name : String, powerType : PowerType.PowerType, id : UniquePowerIdentifier, preCharm :
  Option[UniquePowerIdentifier])
    extends Command {
    protected override def validateInternal(aggregate: Map[UniquePowerIdentifier, Power]): Try[Event] = {
      if (aggregate.contains(id)) {
        Failure(new Exception(s"UniquePowerIdentifier $id already exists in aggregate"))
      } else {
        if (powerType == PowerType.CharmRepurchase) {
          if (preCharm.isDefined) {
            if (aggregate.get(preCharm.get).exists(_.isInstanceOf[Charm])) {
              Success(Creation(owner, name, powerType, id, preCharm))
            } else {
              Failure(new Exception("Charm for which this is a Repurchase does not exist"))
            }
          } else {
            Failure(new Exception("Repurchase Charm request, but no given precursor Charm"))
          }
        } else {
        Success(Creation(owner, name, powerType, id, preCharm))
        }
      }
    }

    override def verifyAccess(user: Email, aggregate: Map[UniquePowerIdentifier, Power]): Boolean = owner.email == user.email
  }


  case class Creation(owner : Email, name : String, powerType : PowerType.PowerType, id : UniquePowerIdentifier, preCharm :
  Option[UniquePowerIdentifier]) extends Event {
    override def commit(aggregate: Map[UniquePowerIdentifier, Power]): Map[UniquePowerIdentifier, Power] = {
      val v : Power = powerType match {
        case PowerType.Charm => Charm(id, name, Description(""), Category("N/A"), Left(Set.empty), Left(Essence(1)),
          Simple, Set.empty, Instant, Set.empty, owner = owner, public = false)
        case PowerType.CharmRepurchase => Repurchase(id, Left(Set.empty), Left(Essence(1)), Simple, aggregate(preCharm
          .get)
          .asInstanceOf[Charm], owner = owner, public = false)
        case PowerType.Evocation => Evocation(id, Set.empty, Left(Set.empty), Instant,Category(""), Simple,  Left(Essence(1)), name,
          Description(""), Set.empty, owner = owner, public = false)
        case PowerType.Spell => Spell(id, Terrestrial, name, Description(""),  Set.empty, Left(Set.empty), Instant, owner = owner, public = false)
      }

      aggregate.+( (id, v))
    }
  }

  case class Publish(id : UniquePowerIdentifier, value : Boolean) extends Command {
    override protected def validateInternal(aggregate: Map[UniquePowerIdentifier, Power]): Try[Event] = {
      ifExists(aggregate, id)(Publication(id, value))
    }
  }

  case class Publication(id : UniquePowerIdentifier, value : Boolean) extends Event {
    override def commit(aggregate: Map[UniquePowerIdentifier, Power]): Map[UniquePowerIdentifier, Power] = {
      aggregate.+((id, aggregate(id).changePublicState(value)))
    }
  }

  case class TransferOwnership(id : UniquePowerIdentifier, newOwner : Email) extends Command {
    override protected def validateInternal(aggregate: Map[UniquePowerIdentifier, Power]): Try[Event] = {
      ifExists(aggregate, id)(OwnershipTransfered(id, newOwner))
    }
  }

  case class OwnershipTransfered(id : UniquePowerIdentifier, newOwner : Email) extends Event {
    override def commit(aggregate: Map[UniquePowerIdentifier, Power]): Map[UniquePowerIdentifier, Power] = {
      aggregate.+((id, aggregate(id).changeOwner(newOwner)))
    }
  }


  case class Delete(id : UniquePowerIdentifier) extends Command {
    protected override def validateInternal(aggregate: Map[UniquePowerIdentifier, Power]): Try[Event] = {
      if (aggregate.contains(id)) {
        Success(Deletion(id))
      } else {
        Failure(new Exception(s"No element with UniquePowerIdentifier $id exists"))
      }
    }
  }



  protected def ifExistsCharm(aggregate: Map[UniquePowerIdentifier, Power], id : UniquePowerIdentifier)(event : =>
  Event) :
  Try[Event]  = {
    if (aggregate.contains(id) && aggregate(id).isInstanceOf[Charm]) {
      Success(event)
    } else {
      Failure(new Exception(s"No element with UniquePowerIdentifier $id exists"))
    }
  }

  protected def ifExists(aggregate: Map[UniquePowerIdentifier, Power], id : UniquePowerIdentifier)(event : => Event) :
  Try[Event]  = {
    if (aggregate.contains(id) && aggregate(id).isInstanceOf[Power]) {
      Success(event)
    } else {
      Failure(new Exception(s"No element with UniquePowerIdentifier $id exists"))
    }
  }


  protected def ifExistsSpell(aggregate: Map[UniquePowerIdentifier, Power], id : UniquePowerIdentifier)(event : => Event) :
  Try[Event]  = {
    if (aggregate.contains(id) && aggregate(id).isInstanceOf[Spell]) {
      Success(event)
    } else {
      Failure(new Exception(s"No element with UniquePowerIdentifier $id exists"))
    }
  }


  protected def ifExistsWithCategory(aggregate: Map[UniquePowerIdentifier, Power], id : UniquePowerIdentifier)(event : => Event) :
  Try[Event]  = {
    if (aggregate.contains(id) && aggregate(id).isInstanceOf[PowerWithTypeAndMins]) {
      Success(event)
    } else {
      Failure(new Exception(s"No element with UniquePowerIdentifier $id exists"))
    }
  }


  protected def ifExistsMins(aggregate: Map[UniquePowerIdentifier, Power], id : UniquePowerIdentifier)(event : => Event) :
  Try[Event]  = {
    if (aggregate.contains(id) && aggregate(id).isInstanceOf[PowerWithTypeAndMins]) {
      Success(event)
    } else {
      Failure(new Exception(s"No element with UniquePowerIdentifier $id exists"))
    }
  }



  case class Deletion(id : UniquePowerIdentifier) extends Event {
    override def commit(aggregate: Map[UniquePowerIdentifier, Power]): Map[UniquePowerIdentifier, Power] = {
      aggregate.-(id)
    }
  }

  case class SetName(id : UniquePowerIdentifier, name : String) extends Command {
    protected override def validateInternal(aggregate: Map[UniquePowerIdentifier, Power]): Try[Event] = {
        ifExists(aggregate, id){NameChanged(id, name)}
    }
  }

  case class NameChanged(id : UniquePowerIdentifier, name : String) extends Event {
    override def commit(aggregate: Map[UniquePowerIdentifier, Power]): Map[UniquePowerIdentifier, Power] = {
      aggregate.+((id, aggregate(id).changeName(name)))
    }
  }

  case class SetDescription(id : UniquePowerIdentifier, description : String) extends Command {
    protected override def validateInternal(aggregate: Map[UniquePowerIdentifier, Power]): Try[Event] = ifExists(aggregate,id)
    {DescriptionChanged(id, description)}
  }

  case class DescriptionChanged(id : UniquePowerIdentifier, description : String) extends Event {
    override def commit(aggregate: Map[UniquePowerIdentifier, Power]): Map[UniquePowerIdentifier, Power] = {
      aggregate.+((id, aggregate(id).changeDescription(description)))
    }
  }

  case class SetCategory(id : UniquePowerIdentifier, category : Category) extends Command {
    protected override def validateInternal(aggregate: Map[UniquePowerIdentifier, Power]): Try[Event] = {
      ifExistsWithCategory(aggregate, id)(CategoryChanged(id, category))
    }
  }

  case class CategoryChanged(id : UniquePowerIdentifier, category : Category) extends Event {
    override def commit(aggregate: Map[UniquePowerIdentifier, Power]): Map[UniquePowerIdentifier, Power] = {
      aggregate.+((id, aggregate(id).asInstanceOf[PowerWithCategory].changeCategory(category)))
    }
  }

  case class SetCost(id : UniquePowerIdentifier, cost : Either[Set[Cost], (Set[Cost], Set[Cost])]) extends Command {
    protected override def validateInternal(aggregate: Map[UniquePowerIdentifier, Power]): Try[Event] = {
      ifExists(aggregate, id)(CostChanged(id, cost))
    }
  }

  case class CostChanged(id : UniquePowerIdentifier, cost : Either[Set[Cost], (Set[Cost], Set[Cost])]) extends Event {
    override def commit(aggregate: Map[UniquePowerIdentifier, Power]): Map[UniquePowerIdentifier, Power] = {
      aggregate.+((id, aggregate(id).changeCost(cost)))
    }
  }

  case class SetMins(id : UniquePowerIdentifier, mins : Either[Essence, (Essence, Ability)]) extends Command {
    protected override def validateInternal(aggregate: Map[UniquePowerIdentifier, Power]): Try[Event] = {
      ifExistsMins(aggregate, id)(MinsChanged(id, mins))
    }
  }

  case class MinsChanged(id : UniquePowerIdentifier, mins : Either[Essence, (Essence, Ability)]) extends Event {
    override def commit(aggregate: Map[UniquePowerIdentifier, Power]): Map[UniquePowerIdentifier, Power] = {
      aggregate.+((id, aggregate(id).asInstanceOf[PowerWithTypeAndMins].changeMins(mins)))
    }
  }

  case class SetType(id : UniquePowerIdentifier, typ : CharmType) extends Command {
    protected override def validateInternal(aggregate: Map[UniquePowerIdentifier, Power]): Try[Event] = {
      ifExistsMins(aggregate, id)(TypeChanged(id, typ))
    }
  }

  case class TypeChanged(id : UniquePowerIdentifier, typ : CharmType) extends Event {
    override def commit(aggregate: Map[UniquePowerIdentifier, Power]): Map[UniquePowerIdentifier, Power] = {
      aggregate.+((id, aggregate(id).asInstanceOf[PowerWithTypeAndMins].changeTyp(typ)))
    }
  }

  case class AddKeyword(id : UniquePowerIdentifier, keyword : String) extends Command {
  protected override def validateInternal(aggregate: Map[UniquePowerIdentifier, Power]): Try[Event] = {
    aggregate.get(id).filterNot(o => o.keywords.contains(Keyword(keyword))).map(c => Success(KeywordAdded(id, keyword)))
      .getOrElse(Failure(new Exception("Cannot add keyword")))
  }
}

  case class KeywordAdded(id : UniquePowerIdentifier, keyword : String) extends Event {
    override def commit(aggregate: Map[UniquePowerIdentifier, Power]): Map[UniquePowerIdentifier, Power] = {
      val newset : Set[Keyword] = aggregate(id).keywords + Keyword(keyword)
      aggregate.+((id, aggregate(id).changeKeywords(newset)))
    }
  }

  case class RemoveKeyword(id : UniquePowerIdentifier, keyword : String) extends Command {
    protected override def validateInternal(aggregate: Map[UniquePowerIdentifier, Power]): Try[Event] = {
      aggregate.get(id).filter(o => o.keywords.contains(Keyword(keyword))).map(c => Success(KeywordRemoved(id, keyword)))
        .getOrElse(Failure(new Exception("Cannot remove keyword")))
    }
  }

  case class KeywordRemoved(id : UniquePowerIdentifier, keyword : String) extends Event {
    override def commit(aggregate: Map[UniquePowerIdentifier, Power]): Map[UniquePowerIdentifier, Power] = {
      val newset : Set[Keyword] = aggregate(id).keywords - Keyword(keyword)
      aggregate.+((id, aggregate(id).changeKeywords(newset)))
    }
  }

  case class SetDuration(id : UniquePowerIdentifier, duration : Duration) extends Command {
    protected override def validateInternal(aggregate: Map[UniquePowerIdentifier, Power]): Try[Event] = {
      ifExists(aggregate,id){DurationChanged(id, duration)}
    }
  }

  case class DurationChanged(id : UniquePowerIdentifier, duration : Duration) extends Event {
    override def commit(aggregate: Map[UniquePowerIdentifier, Power]): Map[UniquePowerIdentifier, Power] = {
      aggregate.+((id, aggregate(id).changeDuration(duration)))
    }
  }

  case class AddPrerequisite(id : UniquePowerIdentifier, prerequisite : Prerequisites) extends Command {
    protected override def validateInternal(aggregate: Map[UniquePowerIdentifier, Power]): Try[Event] = {
      aggregate.get(id).filter(_.isInstanceOf[PowerWithPrereq]).map(_.asInstanceOf[PowerWithPrereq]).filterNot(o => o.prerequisiteCharms
        .contains(prerequisite))
        .map(c => Success
      (PrerequisiteAdded(id,
        prerequisite)))
        .getOrElse(Failure(new Exception("Cannot add prerequisite")))
    }
  }

  case class PrerequisiteAdded(id : UniquePowerIdentifier, prerequisite : Prerequisites) extends Event {
    override def commit(aggregate: Map[UniquePowerIdentifier, Power]): Map[UniquePowerIdentifier, Power] = {
      val newset : Set[Prerequisites] = aggregate(id).asInstanceOf[PowerWithPrereq].prerequisiteCharms + prerequisite
      aggregate.+((id, aggregate(id).asInstanceOf[PowerWithPrereq].changePrerequisites(newset)))
    }
  }

  case class RemovePrerequisite(id : UniquePowerIdentifier, prerequisite : Prerequisites) extends Command {
    protected override def validateInternal(aggregate: Map[UniquePowerIdentifier, Power]): Try[Event] = {
      aggregate.get(id).filter(_.isInstanceOf[PowerWithPrereq]).map(_.asInstanceOf[PowerWithPrereq]).filter(o => o.prerequisiteCharms
        .contains(prerequisite))
        .map(c => Success(PrerequisiteRemoved(id, prerequisite)))
        .getOrElse(Failure(new Exception("Cannot remove prerequisite")))
    }
  }

  case class PrerequisiteRemoved(id : UniquePowerIdentifier, prerequisite : Prerequisites) extends Event {
    override def commit(aggregate: Map[UniquePowerIdentifier, Power]): Map[UniquePowerIdentifier, Power] = {
      val newset : Set[Prerequisites] = aggregate(id).asInstanceOf[PowerWithPrereq].prerequisiteCharms - prerequisite
      aggregate.+((id, aggregate(id).asInstanceOf[PowerWithPrereq].changePrerequisites(newset)))
    }
  }

  case class SetCircle(id : UniquePowerIdentifier, circle : SorceryCircle) extends Command {
    protected override def validateInternal(aggregate: Map[UniquePowerIdentifier, Power]): Try[Event] = {
      ifExistsSpell(aggregate, id)(CircleChanged(id, circle))
    }
  }

  case class CircleChanged(id : UniquePowerIdentifier, circle : SorceryCircle) extends Event {
    override def commit(aggregate: Map[UniquePowerIdentifier, Power]): Map[UniquePowerIdentifier, Power] = {
      aggregate.+((id, aggregate(id).asInstanceOf[Spell].changeCircle(circle)))
    }
  }



  object Serialization {

    final case class FailableEventList(results : Seq[Either[Event, String]])

    import upickle.default._

    def parseJsonToCommands(json : String) : Seq[Command] = read[Seq[Command]](json)
    def parseJsonToEvents(json : String) : Seq[Event] = read[Seq[Event]](json)
    def parseJsonToPowers(json : String) : Seq[Power] = read[Seq[Power]](json)

  }
}
