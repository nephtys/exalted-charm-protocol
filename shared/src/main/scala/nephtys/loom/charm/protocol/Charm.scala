package nephtys.loom.charm.protocol

import upickle.Js

import scala.scalajs.js
import js.annotation.{JSExport, JSExportAll}

/**
  * Created by nephtys on 11/3/16.
  */
@JSExport
@JSExportAll
final case class Charm(
                  uuid : UniquePowerIdentifier,
                  name : String,
                  description : Description,
                  category : Category,
                  cost : Either[Set[Cost], (Set[Cost], Set[Cost])],
                  mins : Either[Essence, (Essence, Ability)],
                  typ : CharmType,
                  keywords : Set[Keyword],
                  duration : Duration,
                  prerequisiteCharms :  Set[Prerequisites],
                  owner : Email,
                  public : Boolean

                ) extends PowerWithTypeAndMins with PowerWithPrereq with PowerWithCategory {

  override def changeName(newname: String) = copy(name = newname)

  override def changeDescription(newDescription: String) = copy(description = Description(newDescription))

  override def changeCategory(newCategory: Category): Power = copy(category = (newCategory))

  override def changeKeywords(newset: Set[Keyword]) = copy(keywords = newset)

  override def changeDuration(newduration: Duration) = copy(duration = newduration)

  override def changeCost(newCost: Either[Set[Cost], (Set[Cost], Set[Cost])]) = copy(cost = newCost)

  override def changeTyp(newtype: CharmType): Power = copy(typ = newtype)

  override def changeMins(newMin: Either[Essence, (Essence, Ability)]): Power = copy(mins = newMin)

  override def changePrerequisites(newset: Set[Prerequisites]): Power = copy(prerequisiteCharms = newset)

  override def readableType: String = "Charm"

  override def changeOwner(newOwner: Email): Power = copy(owner = newOwner)

  override def changePublicState(newValue: Boolean): Power = copy(public = newValue)
}

@JSExport
@JSExportAll
final case class Repurchase(uuid : UniquePowerIdentifier,
                            cost : Either[Set[Cost], (Set[Cost], Set[Cost])],
                            mins : Either[Essence, (Essence, Ability)],
                            typ : CharmType,
                            prerequisiteCharms : Charm //TODO: this currently breaks the whole system
                            ,
                            owner : Email,
                            public : Boolean
                           ) extends PowerWithTypeAndMins {

  override def description: Description = prerequisiteCharms.description

  override def keywords: Set[Keyword] = prerequisiteCharms.keywords

  override def duration: Duration = prerequisiteCharms.duration

  override def name: String = prerequisiteCharms.name

  override def changeName(newname: String): Repurchase.this.type = {
    throw new IllegalArgumentException
  }

  override def changeDescription(newDescription: String) = throw new IllegalArgumentException

  override def changeKeywords(newset: Set[Keyword]) = throw new IllegalArgumentException

  override def changeDuration(newduration: Duration) = throw new IllegalArgumentException

  override def changeCost(newCost: Either[Set[Cost], (Set[Cost], Set[Cost])]) = copy(cost =
    newCost)

  override def changeTyp(newtype: CharmType): Power = copy(typ = newtype)

  override def changeMins(newMin: Either[Essence, (Essence, Ability)]): Power = copy(mins = newMin)

  override def readableType: String = "Charm-Repurchase"

  override def changeOwner(newOwner: Email): Power = copy(owner = newOwner)

  override def changePublicState(newValue: Boolean): Power = copy(public = newValue)

}

@JSExport
final case class Essence (rating : Byte) extends AnyVal

@JSExport
final case class Ability (name : String, rating : Byte)

@JSExport
final case class Keyword(title : String) extends AnyVal

@JSExport
final case class Email(email: String) extends AnyVal

@JSExport
final case class Description(description : String) extends AnyVal

@JSExport
final case class UniquePowerIdentifier(id : String) extends AnyVal

/*
object ProtocolReaderWriter {
  implicit val writerID = upickle.default.Writer[UniquePowerIdentifier]{
    case t => Js.Str(t.id)
  }
  implicit val readerID = upickle.default.Reader[UniquePowerIdentifier]{
    case Js.Str(str) => UniquePowerIdentifier(str)
  }
}*/

sealed trait Cost {
  def number : Int
  def copiedWithNumber(i : Int) : Cost
}

@JSExport
final case class SorcerousMotes(number : Int) extends Cost {
  override def copiedWithNumber(i : Int) = SorcerousMotes(i)
}

@JSExport
final case class Motes(number : Int) extends Cost {
  override def copiedWithNumber(i : Int) = Motes(i)
}

@JSExport
final case class Willpower(number : Int) extends Cost {
  override def copiedWithNumber(i : Int) = Willpower(i)
}

@JSExport
final case class Initiative(number : Int) extends Cost {
  override def copiedWithNumber(i : Int) = Initiative(i)
}

@JSExport
final case class Experience(number : Int) extends Cost {
  override def copiedWithNumber(i : Int) = Experience(i)
}

@JSExport
final case class BashingHealth(number : Int) extends Cost {
  override def copiedWithNumber(i : Int) = BashingHealth(i)
}

@JSExport
final case class LethalHealth(number : Int) extends Cost {
  override def copiedWithNumber(i : Int) = LethalHealth(i)
}

@JSExport
final case class AggravatedHealth(number : Int) extends Cost {
  override def copiedWithNumber(i : Int) = AggravatedHealth(i)
}

@JSExport
final case class Category(str : String) extends AnyVal



sealed trait SorceryCircle

@JSExport
case object Terrestrial extends SorceryCircle

@JSExport
case object Celestial extends SorceryCircle

@JSExport
case object Solar extends SorceryCircle



sealed trait Prerequisites

@JSExport
final case class PrerequisiteCharm(upid : UniquePowerIdentifier) extends Prerequisites

@JSExport
final case class UnrelatedCharmsOfCategory(category : Category, number : Int) extends Prerequisites

@JSExport
final case class UnrelatedCharms(number : Int) extends Prerequisites




sealed trait Duration

@JSExport
case object Instant extends Duration

@JSExport
case object InstantOrUntilEnded extends Duration

@JSExport
case object UntilSunrise extends Duration

@JSExport
case object OneCombat extends Duration

@JSExport
case object OneScene extends Duration

@JSExport
final case class Turns(number : Int) extends Duration

@JSExport
case object Permanent extends Duration with CharmType

@JSExport
case object Reflexive extends Duration with CharmType

@JSExport
case object UntilEnded extends Duration

@JSExport
case object Indefinite extends Duration

@JSExport
case object OneDay extends Duration

@JSExport
final case class One(str : String) extends Duration

@JSExport
final case class Hours(number : Int) extends Duration



sealed trait CharmType
@JSExport
case object Supplemental extends CharmType
@JSExport
case object Simple extends CharmType


@JSExportAll
sealed trait PowerWithCategory extends Power {
  def category : Category
  def changeCategory(newCategory : Category) : Power
}

@JSExportAll
sealed trait PowerWithTypeAndMins extends Power {

  def mins : Either[Essence, (Essence, Ability)]
  def typ : CharmType

  def changeTyp(newtype : CharmType) : Power
  def changeMins(newMins : Either[Essence, (Essence, Ability)]) : Power
}

@JSExportAll
sealed trait PowerWithPrereq extends Power{
  def prerequisiteCharms :  Set[Prerequisites]
  def changePrerequisites(newset : Set[Prerequisites]) : Power
}

@JSExportAll
sealed trait Power {
  def uuid : UniquePowerIdentifier

  def readableType : String

  def name : String
  def description : Description
  def keywords: Set[Keyword]
  def duration : Duration
  def cost : Either[Set[Cost], (Set[Cost], Set[Cost])]
  def public : Boolean
  def owner : Email

  def changeName(newname : String) : Power
  def changeDescription(newDescription : String) : Power
  def changeKeywords(newset : Set[Keyword]) : Power
  def changeDuration(newduration : Duration) : Power
  def changeCost(newCost : Either[Set[Cost], (Set[Cost], Set[Cost])]) : Power
  def changeOwner(newOwner : Email) : Power
  def changePublicState(newValue : Boolean) : Power
}

@JSExport
@JSExportAll
final case class Spell(uuid : UniquePowerIdentifier, circle : SorceryCircle,
                       name : String, description : Description,
                       keywords: Set[Keyword],
                       cost : Either[Set[Cost], (Set[Cost], Set[Cost])],
                       duration : Duration,
                       owner : Email,
                       public : Boolean
                      ) extends Power {
  def changeCircle(circle: SorceryCircle): Spell = copy(circle = circle)


  override def changeName(newname: String) = copy(name = newname)

  override def changeDescription(newDescription: String) = copy(description = Description(newDescription))

  override def changeKeywords(newset: Set[Keyword]) = copy(keywords = newset)

  override def changeDuration(newduration: Duration) = copy(duration = newduration)

  override def changeCost(newCost: Either[Set[Cost], (Set[Cost], Set[Cost])]) = copy(cost = newCost)

  override def readableType: String = "Spell"

  override def changeOwner(newOwner: Email): Power = copy(owner = newOwner)

  override def changePublicState(newValue: Boolean): Power = copy(public = newValue)
}

@JSExport
@JSExportAll
final case class Evocation(uuid : UniquePowerIdentifier,
                           keywords: Set[Keyword],
                           cost : Either[Set[Cost], (Set[Cost], Set[Cost])],
                           duration: Duration,
                           category : Category,
                           typ: CharmType,
                           mins : Either[Essence,(Essence, Ability)], name : String, description : Description,
                           prerequisiteCharms: Set[Prerequisites],
                           owner : Email,
                           public : Boolean

                          )
  extends PowerWithTypeAndMins with PowerWithPrereq with PowerWithCategory {

  override def changeName(newname: String) = copy(name = newname)

  override def changeDescription(newDescription: String) = copy(description = Description(newDescription))

  override def changeKeywords(newset: Set[Keyword]) = copy(keywords = newset)

  override def changeDuration(newduration: Duration) = copy(duration = newduration)

  override def changeCost(newCost: Either[Set[Cost], (Set[Cost], Set[Cost])]) = copy(cost = newCost)

  override def changeTyp(newtype: CharmType): Power = copy(typ = newtype)

  override def changeMins(newMin: Either[Essence, (Essence, Ability)]): Power = copy(mins = newMin)

  override def changePrerequisites(newset: Set[Prerequisites]): Power = copy(prerequisiteCharms = newset)

  override def readableType: String = "Evocation"

  override def changeOwner(newOwner: Email): Power = copy(owner = newOwner)

  override def changeCategory(newCategory: Category): Power = copy(category = (newCategory))

  override def changePublicState(newValue: Boolean): Power = copy(public = newValue)
}

@JSExport
@JSExportAll
object PowerType {
  sealed trait PowerType
  case object Charm extends PowerType
  case object CharmRepurchase extends PowerType
  case object Evocation extends PowerType
  case object Spell extends PowerType
}
